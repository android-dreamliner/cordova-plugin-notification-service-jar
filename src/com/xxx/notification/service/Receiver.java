package com.xxx.notification.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Receiver.java receives an event caller about the device boot or installs
 */

public class Receiver extends BroadcastReceiver {

	// Constants ---------------------------------------------------------------------------------------------- Constants
	private static final String TAG_NAME = "NotificationService"; // logger

	public static final String REFERRER = "referrer"; // 

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables

	// Constructors ---------------------------------------------------------------------------------------- Constructors

	// Public Methods ------------------------------------------------------------------------------------ Public Methods

	@Override
	public void onReceive(Context context, Intent intent) {

		/***** For Notification Service to start on BOOT ****/
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			new NotificationProxy(context).launch();
		}

		/***** For Notification Service when app is installed ****/
		if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
			Log.d(TAG_NAME, "Service: received install referrer event");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String referrerString = extras.getString(REFERRER);
				if (referrerString != null && isValid(referrerString)) {
					SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
					Editor edit = sharedPreferences.edit();
					edit.putString(REFERRER, referrerString);
					edit.commit();
					Log.d(TAG_NAME, "Service: referrer: " + referrerString);
					new NotificationProxy(context).launch();
				}
			}
		}

	}

	//check if referer is valid
	public static boolean isValid(String ref) {
		try {
			String[] refArr = ref.split(":");
			String clickId = refArr[0];
			String hash = refArr[1];
			if (clickId.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")) {
				String realHash = SHA1(AesUtils.decrypt("iGTgQnkA2zH7aPJE4csmnGE/860+xFkNbb6+P2eaPdY=", AesUtils.encryptionKey) + clickId);
				if (realHash != null && realHash.equals(hash)) {
					return true;
				}
			}
		} catch (Exception e) {
			//do nothing
		}
		return false;
	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods
	private static String convertToHex(byte[] data) {
		StringBuilder buf = new StringBuilder();
		for (byte b : data) {
			int halfbyte = (b >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
				halfbyte = b & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		byte[] sha1hash = md.digest();
		return convertToHex(sha1hash);
	}
	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

}
