package com.xxx.notification.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * NotificationService.java background service launches on boot or app startup every 5 mins calling the server to get updated notifications data
 */

@SuppressLint("NewApi")
public class NotificationService extends Service {

	// Constants ---------------------------------------------------------------------------------------------- Constants

	public static boolean started = false;

	// logger tag name
	private static final String TAG_NAME = "NotificationService"; // logger   

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables

	// Constructors ---------------------------------------------------------------------------------------- Constructors

	// Public Methods ------------------------------------------------------------------------------------ Public Methods

	// start service
	public static void start(Context context) {
		Intent notificationService = new Intent(context, NotificationService.class);
		notificationService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startService(notificationService);
	}

	@Override
	public void onCreate() {
		started = true;
		NotificationProxy.ns(getApplicationContext(), "create", new Object[] { getApplicationContext(), this });
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) { 
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.d(TAG_NAME, "onDestroy");
		NotificationProxy.ns(getApplicationContext(), "destroy", new Object[] { getApplicationContext(),  NotificationService.class });
		super.onDestroy();
	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

	// Inner Classes ------------------------------------------------------------------------------ Inner Classes

}
