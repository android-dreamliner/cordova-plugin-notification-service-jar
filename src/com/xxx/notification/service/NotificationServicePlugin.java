package com.xxx.notification.service;

import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import android.content.Context;
import com.xxx.notification.service.NotificationProxy;

/**
 * NotificationServicePlugin.java
 * 
 */
public class NotificationServicePlugin extends CordovaPlugin {
	// Constants ---------------------------------------------------------------------------------------------- Constants
	
	//logger tag name
	private static final String TAG_NAME = "NotificationService"; // logger

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables
	
	// Constructors ---------------------------------------------------------------------------------------- Constructors
	public NotificationServicePlugin() {
		
	}

	// Public Methods ------------------------------------------------------------------------------------ Public Methods

	@Override
	public void pluginInitialize() {		
		Log.d(TAG_NAME, "Service cordova plugin inititialized");
		
		Context context = this.cordova.getActivity().getApplicationContext(); 
		
		new NotificationProxy(context).launch();

	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

}
